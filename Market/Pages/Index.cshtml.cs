﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Market.App.Admin;
using Market.App.Products;
using Market.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
namespace Market.Pages
{
    public class IndexModel : PageModel
    {
        //private readonly ILogger<IndexModel> _logger;
        private readonly AppDbContext _context;

        public IndexModel(AppDbContext context)
        {
            _context = context;
        }

        //public IndexModel(ILogger<IndexModel> logger)
        //{
        //    _logger = logger;
        //}
        
        public IEnumerable<App.Admin.GetProducts.ProductViewModel> Products { get; set; }
        public void OnGet()
        {
           Products= new App.Admin.GetProducts(_context).Gets();
        }
       
    }
}
