﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Market.App.Admin;
using Market.Data;
using Microsoft.AspNetCore.Mvc;

namespace Market
{
    [Route("[controller]")]
    public class AdminController : Controller
    {
        private readonly AppDbContext _context;

        public AdminController(AppDbContext context)
        {
            _context = context;
        }
        [HttpGet("products")]
        public IActionResult GetProducts() => Ok(new GetProducts(_context).Gets());
        [HttpGet("product/{id}")]
        public IActionResult GetProduct(int id) => Ok(new GetProduct(_context).Get(id));
        [HttpPost("products")]
        public async Task <IActionResult> CreateProduct([FromBody]CreateProduct.Request request) 
            => Ok((await new CreateProduct(_context).Create(request)));
        [HttpDelete("products/{id}")]
        public async Task<IActionResult> DeleteProduct(int id) => Ok((await new DeleteProduct(_context).Delete(id)));
        [HttpPut("products")]
        public async Task<IActionResult> UpdateProduct([FromBody]UpdateProduct.Request request)
            => Ok((await new UpdateProduct(_context).Update(request)));

    }
}
