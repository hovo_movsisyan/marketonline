﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Market.Dom.Models
{
    public class Order
    {
        public int Id { get; set; }
        public string OrderRef { get; set; }
        public string AddressFirst { get; set; }
        public string AddressSecond { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public ICollection<ProductOrder> ProductOrders { get; set; }

    }
}
