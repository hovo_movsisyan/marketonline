﻿using Market.Data;
using Market.Dom.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market.App.Admin
{
    public class UpdateProduct
    {
        private readonly AppDbContext _context;

        public UpdateProduct(AppDbContext  context)
        {
            _context = context;
        }
        public async Task<Response> Update(Request request)
        {
            var product = _context.Products.FirstOrDefault(x => x.Id == request.Id);
            product.Name = request.Name;
            product.Description = request.Description;
            product.Price = request.Price;
            await _context.SaveChangesAsync();
            return new Response
            {
                Id=product.Id,
                Name=product.Name,
                Description=product.Description,
                Price=product.Price
            };
        }
        public class Request
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public decimal Price { get; set; }
        }
        public class Response
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public decimal Price { get; set; }
        }
    }
}
