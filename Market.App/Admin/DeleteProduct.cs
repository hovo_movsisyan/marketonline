﻿using Market.Data;
using Market.Dom.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Market.App.Admin
{
    public class DeleteProduct
    {
        private readonly AppDbContext _context;

        public DeleteProduct(AppDbContext  context)
        {
            _context = context;
        }
        public async Task<bool> Delete(int id)
        {
            var product = _context.Products.FirstOrDefault(x => x.Id == id);
            _context.Products.Remove(product);
            await _context.SaveChangesAsync();
            return true;
        }
    }
}
