﻿using Market.Data;
using Market.Dom.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Market.App.Admin
{
    public class CreateProduct
    {
        private readonly AppDbContext _context;

        public CreateProduct(AppDbContext  context)
        {
            _context = context;
        }
        public async Task<Response> Create(Request request)
        {
            var product = new Product
            {
                Price = request.Price,
                Name = request.Name,
                Description = request.Description
            };
            _context.Products.Add(product);
            await _context.SaveChangesAsync();
            return new Response
            {
                Id = product.Id,
                Name = product.Name,
                Description = product.Description,
                Price = product.Price
            };
        }
        public class Request
        {
            public string Name { get; set; }
            public string Description { get; set; }
            public decimal Price { get; set; }
        }
        public class Response
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public decimal Price { get; set; }
        }
    }
}
