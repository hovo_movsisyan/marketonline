﻿using Market.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Market.App.Admin
{
    public class GetProducts
    {
        private readonly AppDbContext _context;

        public GetProducts(AppDbContext context)
        {
            _context = context;
        }
        public IEnumerable<ProductViewModel> Gets() => _context.Products.ToList().Select(x => new ProductViewModel
        {
            Id=x.Id,
            Name = x.Name,
            Price = x.Price
        });

        public class ProductViewModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public decimal Price { get; set; }
        }
    }
}
