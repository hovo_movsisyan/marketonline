﻿using Market.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Market.App.Products
{
    public class GetProducts
    {
        private readonly AppDbContext _context;

        public GetProducts(AppDbContext context)
        {
            _context = context;
        }
        public IEnumerable<ProductViewModel> Gets() => _context.Products.ToList().Select(x => new ProductViewModel
        {
            Name = x.Name,
            Description = x.Description,
            Price = $"$ {x.Price:N2}"
        });

        public class ProductViewModel
        {
            public string Name { get; set; }
            public string Description { get; set; }
            public string Price { get; set; }
        }
    }
}
